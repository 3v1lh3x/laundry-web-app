import Vue from 'vue';
import VueRouter from 'vue-router';

import UserMain from './components/UserMain';
import AdminMain from './components/AdminMain';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',

  routes: [
    {
      path: '/',
      component: UserMain,
    },
    {
      path: '/admin',
      component: AdminMain,
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
